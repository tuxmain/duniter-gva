//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use anyhow::Context;
use read_input::prelude::*;
use serde::{Deserialize, Serialize};
use std::{
    collections::HashSet,
    net::{IpAddr, Ipv4Addr, Ipv6Addr},
};
use structopt::StructOpt;

const MODULE_NAME: &str = "gva";

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct GvaConf {
    pub enabled: bool,
    #[serde(default = "ip4_default")]
    pub ip4: Ipv4Addr,
    pub ip6: Option<Ipv6Addr>,
    #[serde(default = "port_default")]
    pub port: u16,
    #[serde(default = "path_default")]
    pub path: String,
    #[serde(default = "subscriptions_path_default")]
    pub subscriptions_path: String,
    pub remote_host: Option<String>,
    pub remote_port: Option<u16>,
    pub remote_path: Option<String>,
    pub remote_subscriptions_path: Option<String>,
    pub remote_tls: Option<bool>,
    #[serde(default = "whitelist_default")]
    pub whitelist: Vec<IpAddr>,
}

const fn ip4_default() -> Ipv4Addr {
    Ipv4Addr::UNSPECIFIED
}

const fn ip6_default() -> Ipv6Addr {
    Ipv6Addr::UNSPECIFIED
}

fn path_default() -> String {
    "gva".to_owned()
}

const fn port_default() -> u16 {
    30_901
}

fn subscriptions_path_default() -> String {
    "gva-sub".to_owned()
}

fn whitelist_default() -> Vec<IpAddr> {
    vec![
        IpAddr::V4(Ipv4Addr::LOCALHOST),
        IpAddr::V6(Ipv6Addr::LOCALHOST),
    ]
}

impl Default for GvaConf {
    fn default() -> Self {
        GvaConf {
            enabled: false,
            ip4: ip4_default(),
            ip6: Some(ip6_default()),
            port: port_default(),
            path: path_default(),
            subscriptions_path: subscriptions_path_default(),
            remote_host: None,
            remote_port: None,
            remote_path: None,
            remote_subscriptions_path: None,
            remote_tls: None,
            whitelist: whitelist_default(),
        }
    }
}

impl GvaConf {
    pub fn get_remote_port(&self) -> u16 {
        self.remote_port.unwrap_or(self.port)
    }
    pub fn get_remote_path(&self) -> String {
        self.remote_path
            .clone()
            .unwrap_or_else(|| self.path.clone())
    }
    pub fn get_remote_subscriptions_path(&self) -> String {
        self.remote_subscriptions_path
            .clone()
            .unwrap_or_else(|| self.subscriptions_path.clone())
    }
}

#[derive(StructOpt)]
pub enum GvaCommand {
    Configure,
}

impl GvaCommand {
    pub fn command(self, profile_path: std::path::PathBuf) -> anyhow::Result<()> {
        match self {
            GvaCommand::Configure => {
                let gva_conf =
                    duniter_core::conf::load_module_conf(MODULE_NAME, &Some(profile_path.clone()))
                        .context("fail to load gva conf")?;

                let new_gva_conf = GvaCommand::configure(gva_conf);

                duniter_core::conf::write_module_conf(new_gva_conf, MODULE_NAME, &profile_path)
                    .context("fail to write gva conf")?;
            }
        }

        Ok(())
    }

    fn configure(mut conf: GvaConf) -> GvaConf {
        // Enable GVA API?
        let res = input().msg("Enable GVA API? [Y/n]").default('Y').get();
        conf.enabled = res != 'n';

        if conf.enabled {
            // ip4
            conf.ip4 = input()
                .msg(format!("Listen to ip v4 ? [{}]", conf.ip4))
                .default(conf.ip4)
                .get();
            // ip6
            let res = input().msg("Listen to ip v6? [Y/n]").default('Y').get();
            if res != 'n' {
                let proposed_ip6 = conf.ip6.unwrap_or(Ipv6Addr::UNSPECIFIED);
                conf.ip6 = Some(
                    input()
                        .msg(format!("Enter ip v6: [{}]", proposed_ip6))
                        .default(proposed_ip6)
                        .get(),
                );
            } else {
                conf.ip6 = None;
            }
            // port
            conf.port = input()
                .msg(format!("Listen to port ? [{}]", conf.port))
                .default(conf.port)
                .get();
            // path
            conf.path = input()
                .msg(format!("Path ? [{}]", conf.path))
                .default(conf.path)
                .get();
            // subscriptionsPath
            conf.subscriptions_path = input()
                .msg(format!(
                    "Subscriptions path ? [{}]",
                    conf.subscriptions_path
                ))
                .default(conf.subscriptions_path)
                .get();
            // remoteHost
            if let Some(ref remote_host) = conf.remote_host {
                let new_remote_host = input()
                    .msg(format!("Remote host ? [{}]", remote_host))
                    .default(remote_host.to_owned())
                    .get();
                if !new_remote_host.is_empty() {
                    conf.remote_host = Some(new_remote_host);
                }
            } else {
                let new_remote_host: String = input().msg("Remote host ? ").get();
                if !new_remote_host.is_empty() {
                    conf.remote_host = Some(new_remote_host);
                }
            }
            // remotePort
            let res = input()
                .msg("Define a remote port? [y/N]")
                .default('N')
                .get();
            if res == 'y' || res == 'Y' {
                let default = conf.remote_port.unwrap_or(443);
                conf.remote_port = Some(
                    input()
                        .msg(format!("Remote port ? [{}]", default))
                        .default(default)
                        .get(),
                );
            } else {
                conf.remote_port = None;
            }
            // remotePath
            let res = input()
                .msg("Define a remote path? [y/N]")
                .default('N')
                .get();
            if res == 'y' || res == 'Y' {
                conf.remote_path = Some(input().msg("Enter remote path:").get());
            } else {
                conf.remote_path = None;
            }
            // remoteSubscriptionsPath
            let res = input()
                .msg("Define a remote subscriptions path? [y/N]")
                .default('N')
                .get();
            if res == 'y' || res == 'Y' {
                conf.remote_subscriptions_path =
                    Some(input().msg("Enter remote subscriptions path:").get());
            } else {
                conf.remote_subscriptions_path = None;
            }
            // whitelist
            let mut whitelist: HashSet<_> = conf.whitelist.iter().copied().collect();
            let res = input().msg("Update whitelist? [y/N]").default('N').get();
            if res == 'y' || res == 'Y' {
                loop {
                    println!("1. See whitelist content.");
                    println!("2. Add an IP to the whitelist.");
                    println!("3. Removing an IP from the whitelist.");
                    println!("4. Quit.");
                    match input().msg("Choose an action: ").default(1).get() {
                        2usize => {
                            whitelist.insert(input().msg("Enter a new IP address: ").get());
                        }
                        3 => {
                            whitelist.remove(
                                &input().msg("Indicate the IP address to be deleted: ").get(),
                            );
                        }
                        4 => break,
                        _ => {
                            println!("--------------------------------");
                            println!("Whitelist content ({} IPs):", whitelist.len());
                            whitelist.iter().for_each(|ip| println!("{}", ip));
                            println!("--------------------------------");
                        }
                    }
                }
            }
            conf.whitelist = whitelist.iter().copied().collect();
        }

        println!("Configuration successfully updated.");

        conf
    }
}
