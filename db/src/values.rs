//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;

pub mod gva_block_db;
pub mod gva_idty_db;
pub mod gva_tx;
pub mod wallet_script_array;

#[derive(Clone, Copy, Debug, Default, PartialEq, Serialize, Deserialize)]
pub struct HashDb(pub Hash);

impl AsBytes for HashDb {
    fn as_bytes<T, F: FnMut(&[u8]) -> T>(&self, mut f: F) -> T {
        f(self.0.as_ref())
    }
}

impl kv_typed::prelude::FromBytes for HashDb {
    type Err = bincode::Error;

    fn from_bytes(bytes: &[u8]) -> std::result::Result<Self, Self::Err> {
        let mut hash_bytes = [0; 32];
        hash_bytes.copy_from_slice(bytes);
        Ok(Self(Hash(hash_bytes)))
    }
}

impl ToDumpString for HashDb {
    fn to_dump_string(&self) -> String {
        todo!()
    }
}

#[cfg(feature = "explorer")]
impl ExplorableValue for HashDb {
    fn from_explorer_str(source: &str) -> Result<Self, FromExplorerValueErr> {
        Ok(Self(
            Hash::from_hex(source).map_err(|e| FromExplorerValueErr(e.into()))?,
        ))
    }
    fn to_explorer_json(&self) -> KvResult<serde_json::Value> {
        Ok(serde_json::Value::String(self.0.to_hex()))
    }
}
