# Duniter GVA

This repository contains the code of the GVA module.

## Duniter repositories

Duniter's code is separated into several git repositories:

* **[dubp-rs-libs]** contains the logic common to Duniter and its customers.
* **[duniter-core]** contains the core code of Duniter.
* The gitlab subgroup **[nodes/rust/modules]** contains the main Duniter modules code (gva, admin, etc).
* The **[duniter]** subgroup contains the "official" implementations of the "duniter-cli" and "duniter-desktop" programs with their default modules (also contains the historical implementation being migrated).

[DuniterModule]: https://git.duniter.org/nodes/rust/duniter-core/blob/main/module/src/lib.rs#L41
[duniter gitlab]: https://git.duniter.org
[dubp-rs-libs]: https://git.duniter.org/libs/dubp-rs-libs
[duniter-core]: https://git.duniter.org/nodes/rust/duniter-core
[duniter]: https://git.duniter.org/nodes/typescript/duniter
[nodes/rust/modules]: https://git.duniter.org/nodes/rust/modules
