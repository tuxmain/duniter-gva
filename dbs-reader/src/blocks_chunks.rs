//  Copyright (C) 2021 Pascal Engélibert
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;
use flate2::read::ZlibDecoder;

pub const CHUNK_FILE_PREFIX: &str = "_";
pub const CHUNK_FILE_EXT: &str = ".bin.gz";

/// Read and decompress chunk file
pub fn read_compressed_chunk(
    chunk_index: u32,
    chunks_folder_path: &Path,
    remove: bool,
) -> std::io::Result<Option<Vec<u8>>> {
    let file_path = chunks_folder_path.join(format!(
        "{}{}{}",
        CHUNK_FILE_PREFIX, chunk_index, CHUNK_FILE_EXT
    ));
    if !file_path.exists() {
        return Ok(None);
    }
    if std::fs::metadata(file_path.as_path())?.len() > 0 {
        let file = std::fs::File::open(file_path.as_path())?;
        let mut z = ZlibDecoder::new(file);
        let mut decompressed_bytes = Vec::new();
        z.read_to_end(&mut decompressed_bytes)?;

        if remove {
            std::fs::remove_file(file_path)?;
        }
        Ok(Some(decompressed_bytes))
    } else {
        if remove {
            std::fs::remove_file(file_path)?;
        }
        Ok(None)
    }
}
